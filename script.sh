# Initialize a new Git repository in the current directory:
git init

# Add all files in the current directory to the staging area:
git add .

# Sets the global user email configuration 
# replace "you@something.com" with your own email id:
git config --global user.email "muhammad.umair@educative.io"

# Sets the global user name configuration
# replace "UserName" with your own user name:
git config --global user.name "muhammad.umair4882918"

# Commits the changes to the local repository with a 
# commit message of "test":
git commit -m "Running pipeline"

# Adds a remote repository named "origin" 
# replace <remote repository URL> with your own gitlab URL:
git remote add origin https://gitlab.com/muhammad.umair4882918/gitlab_educative.git

# Pushes the changes to the remote repository 
# named "origin" on the "master" branch:
git push -u origin main