package main
import "fmt"

func FibonacciSeriesFunc(num int){
   i := 0
   j := 1
   k := j
   fmt.Printf("Series is: %d %d", i, j)
   for true{
      k = j
      j = i + j
      if j >= num{
         fmt.Println()
         break
      }
      i = k
      fmt.Printf(" %d", j)
   }
}

func main(){
   FibonacciSeriesFunc(10)
}